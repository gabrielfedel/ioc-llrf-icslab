#- Common Configuration (iocStats + timing)
#- iocStats Configuration
iocshLoad $(E3_CMD_TOP)/iocStats.iocsh

#- Timing Configuration
iocshLoad $(E3_CMD_TOP)/timing.iocsh
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES" "16777300")
require sis8300llrf
ndsSetTraceLevel 3

#- Hardware specific configuration (can repeat n times)
epicsEnvSet("LLRF_PREFIX"   "LLRF-1")
epicsEnvSet("LLRF_SLOT",    "11")
iocshLoad $(E3_CMD_TOP)/llrf.iocsh
epicsEnvSet("LLRF_PREFIX_BASE"  "LLRF")
iocshLoad $(E3_CMD_TOP)/config-aliasing.iocsh
iocshLoad $(E3_CMD_TOP)/config-desc.iocsh

#- Export module versions used to file
afterInit("dbgrep REQMOD* > '/tmp/REQMODs.list'")
